#### The Hacker (Fool)
- Beginning
- Spontaneity
- Faith
- Folly

#### The Engineer (Magician)
- Action
- Conscious Awareness
- Concentration
- Power

#### The Dreamer (High Priestess)
- Inaction
- Unconscious Awareness
- Potential
- Mystery

#### The People (Empress)
- Nurturing
- Abundance
- Senses
- Liberty

#### The State (Emperor)
- Leadership
- Structure
- Authority
- Regulation

#### The Socialist (Heirophant)
- Education
- Culture
- Conformity
- Belonging

#### The Capitalist (Additional)
- Materialism
- Selfish Individuality
- Betrayal
- Accumulation

#### The Military (Chariot)
- Victory
- Will
- Assertion
- Hard Control

#### The Anarchist (Strength)
- Strength
- Endurance
- Compassion
- Soft Control

#### The Lovers (Lovers)
- Relationship
- Sexuality
- Ethics
- Personal Values

#### The Slut (Additional)
- Temptation
- Pleasure
- Hedonism
- Personal Liberty

#### The Scientist (Hermit)
- Introspection
- Searching
- Guiding
- Knowledge

#### The Layoff (Hanged Man)
- Reversing
- Suspending Action
- Sacrifice
- Loss

#### The Transhuman (Temperance)
- Advancing
- Balance
- Health
- Combination

#### The Prisoner (Devil)
- Bondage
- Solitude
- Ignorance
- Hopelessness

#### The Artist (Star)
- Hope
- Inspiration
- Greatness
- Imagination

#### Death (Death)
- Ending
- Deletion
- Transition
- Inevitability

#### The Net (World)
- Integration
- Connection
- Information
- Conversation

#### The Encrypted (Additional)
- Unknown

#### The Advocate (Sun)
- Enlightenment
- Justice
- Vitality
- Assurance

#### The Agent (Moon)
- Fear
- Illusion
- Deception
- Bewilderment

#### The Explosive
- Sudden Change
- Release
- Destruction
- Revelation

#### The Terminal (Wheel of Fortune)
- Destiny
- Turning Point
- Movement
- Personal Vision

#### The Butterfly Effect (Justice)
- Responsibility
- Decision
- Causality
- Significance

#### The Singularity (Judgment)
- Judgment
- Rebirth
- Call
- Absolution

#### The Corporation (Additional)
- Massiveness
- Purposefulness
- Organization
- Influence

#### The Drone (Additional)
- Obedience
- Servitude
- Work
- Expendability

#### The Sprawl (Additional)
- Chaos
- Disorganization
- Improvisation
- Hidden

#### The Freelancer (Additional)
- Goals
- Survival
- Mercenary
- Independence

#### The Mechanical (Additional)
- Cold/Rigid
- Precise
- Effective
- Efficient

#### The Biological (Additional)
- Warmth
- Evolution
- Growth
- Flexibility

#### The Smog (Additional)
- Illness
- Unclarity
- Difficulty
- Hindrance
